# -*- coding: utf-8 -*-

# CoreSEC Softwares, Ca
# Mark Anthony Pequeras
# Licensed Under MIT License
# InvictuzMU/BeastMU Online Launcher
# www.coresecsoftware.com/python/BeastLauncher

from PyQt4 import QtCore, QtGui
from subprocess import Popen
import urllib, urllib2

try:
    _fromUtf8 = QtCore.QString.fromUtf8

except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Form(object):
    def __init__(self):
        # Options Cache for GUI Objects
        import ConfigParser

        try:
            _file = ["Settings/Main","BeastLauncher"]
            _Parser = ConfigParser.ConfigParser()
            _Parser.read(_file[0])
        except:
            self.BeastException(title="OptionsError",message="Unable to Load Options!")
        self.Getter = lambda s,v: _Parser.get(s,v) if True else None
        self.Setter = lambda s,o,v: _Parser.set(s,o,v)
        self.RemCurZip()

    def RemCurZip(self,version=None):
        import os
        from_opt = self.Getter("BeastUpdater","version")
        _n = str(from_opt)+'.zip'
        try:
            os.remove(_n)
        except:
            pass

    def setupUi(self, Form):
        QtCore.QCoreApplication.addLibraryPath('coresec')
        self.booster = QtGui.qApp


        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(906, 480)
        Form.setMinimumSize(QtCore.QSize(906, 480))
        Form.setMaximumSize(QtCore.QSize(906, 480))
        Form.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        Form.setStyleSheet(_fromUtf8("background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #26293B , stop: 1 #333751);"))

        self.OrangeBar = QtGui.QPlainTextEdit(Form)
        self.OrangeBar.setEnabled(False)
        self.OrangeBar.setGeometry(QtCore.QRect(70, 20, 641, 311))
        self.OrangeBar.setStyleSheet(_fromUtf8("border-radius: 15px 33px 22px 33px;\n"
"border: 3px outset #333751;\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #26293B , stop: 1 #26293B);"))
        self.OrangeBar.setObjectName(_fromUtf8("OrangeBar"))

        self.SMScredit = QtGui.QLabel(Form)
        self.SMScredit.setGeometry(QtCore.QRect(720, 210, 141, 91))
        self.SMScredit.setStyleSheet(_fromUtf8("background: transparent;\n"
""))
        self.SMScredit.setText(_fromUtf8(""))
        self.SMScredit.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/Settings/CSImages/smscreds.jpg")))
        self.SMScredit.setObjectName(_fromUtf8("SMScredit"))

        self.label_3 = QtGui.QLabel(Form)
        self.label_3.setGeometry(QtCore.QRect(720, 310, 131, 51))
        self.label_3.setStyleSheet(_fromUtf8("background: transparent;"))
        self.label_3.setText(_fromUtf8(""))
        self.label_3.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/Settings/CSImages/webshop.jpg")))
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName(_fromUtf8("label_3"))

        self.MuLogoTop = QtGui.QLabel(Form)
        self.MuLogoTop.setGeometry(QtCore.QRect(350, 0, 201, 71))
        self.MuLogoTop.setStyleSheet(_fromUtf8("background: transparent;"))
        self.MuLogoTop.setText(_fromUtf8(""))
        self.MuLogoTop.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/Settings/CSImages/reg_logo.png")))
        self.MuLogoTop.setObjectName(_fromUtf8("MuLogoTop"))

        self.redplus = QtGui.QLabel(Form)
        self.redplus.setGeometry(QtCore.QRect(680, 30, 16, 16))
        self.redplus.setStyleSheet(_fromUtf8("background: transparent;"))
        self.redplus.setText(_fromUtf8(""))
        self.redplus.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/Settings/CSImages/arrow.gif")))
        self.redplus.setObjectName(_fromUtf8("redplus"))

        self.WebshopButton = QtGui.QPushButton(Form)
        self.WebshopButton.setGeometry(QtCore.QRect(720, 310, 131, 51))
        self.WebshopButton.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.WebshopButton.setStyleSheet(_fromUtf8("border-image: url(:/images/Settings/CSImages/round.gif);\n"
"background: transparent;"))
        self.WebshopButton.setText(_fromUtf8(""))
        self.WebshopButton.setObjectName(_fromUtf8("WebshopButton"))

        self.SMSCreditsButtons = QtGui.QPushButton(Form)
        self.SMSCreditsButtons.setGeometry(QtCore.QRect(720, 220, 141, 71))
        self.SMSCreditsButtons.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.SMSCreditsButtons.setStyleSheet(_fromUtf8("border-image: url(:/images/Settings/CSImages/round.gif);\n"
"background: transparent;"))
        self.SMSCreditsButtons.setText(_fromUtf8(""))
        self.SMSCreditsButtons.setObjectName(_fromUtf8("SMSCreditsButtons"))

        self.VoteButton = QtGui.QPushButton(Form)
        self.VoteButton.setGeometry(QtCore.QRect(720, 140, 151, 61))
        self.VoteButton.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.VoteButton.setStyleSheet(_fromUtf8("\n"
"background: transparent;\n"
"border-image: url(:/images/Settings/CSImages/votebanner.jpg);"))
        self.VoteButton.setText(_fromUtf8(""))
        self.VoteButton.setObjectName(_fromUtf8("VoteButton"))

        eslide = self.Getter("BeastFront","Slider")
        enews = self.Getter("BeastFront","News")

        self.WebViewScrollingImages = QtWebKit.QWebView(Form)
        self.WebViewScrollingImages.setGeometry(QtCore.QRect(190, 70, 251, 251))
        self.WebViewScrollingImages.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.WebViewScrollingImages.setStyleSheet(_fromUtf8("background: transparent;\n"
""))
        self.WebViewScrollingImages.setUrl(QtCore.QUrl(_fromUtf8(str(eslide))))
        self.WebViewScrollingImages.setRenderHints(QtGui.QPainter.Antialiasing|QtGui.QPainter.SmoothPixmapTransform|QtGui.QPainter.TextAntialiasing)
        self.WebViewScrollingImages.setObjectName(_fromUtf8("WebViewScrollingImages"))

        self.WebViewNewsEvents = QtWebKit.QWebView(Form)
        self.WebViewNewsEvents.setGeometry(QtCore.QRect(450, 70, 251, 251))
        self.WebViewNewsEvents.setUrl(QtCore.QUrl(_fromUtf8(str(enews))))
        self.WebViewNewsEvents.setRenderHints(QtGui.QPainter.Antialiasing|QtGui.QPainter.HighQualityAntialiasing|QtGui.QPainter.NonCosmeticDefaultPen|QtGui.QPainter.SmoothPixmapTransform|QtGui.QPainter.TextAntialiasing)
        self.WebViewNewsEvents.setObjectName(_fromUtf8("WebViewNewsEvents"))

        self.LatestVersionName = QtGui.QLabel(Form)
        self.LatestVersionName.setGeometry(QtCore.QRect(720, 380, 81, 16))
        self.LatestVersionName.setStyleSheet(_fromUtf8("color: rgb(145, 145, 145);"))
        self.LatestVersionName.setObjectName(_fromUtf8("LatestVersionName"))

        self.YourVersionName = QtGui.QLabel(Form)
        self.YourVersionName.setGeometry(QtCore.QRect(720, 400, 81, 16))
        self.YourVersionName.setStyleSheet(_fromUtf8("color: rgb(145, 145, 145);"))
        self.YourVersionName.setObjectName(_fromUtf8("YourVersionName"))

        self.StatusName = QtGui.QLabel(Form)
        self.StatusName.setGeometry(QtCore.QRect(720, 420, 61, 20))
        self.StatusName.setStyleSheet(_fromUtf8("color: rgb(145, 145, 145);"))
        self.StatusName.setObjectName(_fromUtf8("StatusName"))

        self.label_11 = QtGui.QLabel(Form)
        self.label_11.setGeometry(QtCore.QRect(780, 420, 81, 16))
        self.label_11.setStyleSheet(_fromUtf8("color: rgb(188, 188, 188);"))
        self.label_11.setObjectName(_fromUtf8("label_11"))

        self.NotificationBox = QtGui.QPlainTextEdit(Form)
        self.NotificationBox.setGeometry(QtCore.QRect(320, 330, 381, 21))

        font = QtGui.QFont()
        font.setPointSize(7)

        self.NotificationBox.setFont(font)
        self.NotificationBox.setStyleSheet(_fromUtf8("color: rgb(0, 170, 0);\n"
"background: transparent;"
"border: none;"))
        self.NotificationBox.insertPlainText(_fromUtf8(""))
        self.NotificationBox.setObjectName(_fromUtf8("NotificationBox"))
        self.NotificationBox.setDisabled(1)

        self.NotificationBox2 = QtGui.QPlainTextEdit(Form)
        self.NotificationBox2.setGeometry(QtCore.QRect(320, 330, 381, 21))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.NotificationBox2.setFont(font)
        self.NotificationBox2.setStyleSheet(_fromUtf8("color: rgb(0, 170, 0);\n"
"background: transparent;"
"border: none;"))
        self.NotificationBox2.insertPlainText(_fromUtf8(""))
        self.NotificationBox2.setObjectName(_fromUtf8("NotificationBox2"))
        self.NotificationBox2.setDisabled(1)


        self.ServerName = QtGui.QLabel(Form)
        self.ServerName.setGeometry(QtCore.QRect(100, 20, 251, 51))

        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Monotype Corsiva"))
        font.setPointSize(24)
        font.setBold(True)
        font.setItalic(True)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)

        self.ServerName.setFont(font)
        self.ServerName.setStyleSheet(_fromUtf8("background: none;\n"
"\n"
"\n"
"\n"
"color: qconicalgradient(cx:0, cy:0, angle:135, stop:0 rgba(255, 255, 0, 69), stop:0.375 rgba(255, 255, 0, 69), stop:0.423533 rgba(251, 255, 0, 145), stop:0.45 rgba(247, 255, 0, 208), stop:0.477581 rgba(255, 244, 71, 130), stop:0.518717 rgba(255, 218, 71, 130), stop:0.55 rgba(255, 255, 0, 255), stop:0.57754 rgba(255, 203, 0, 130), stop:0.625 rgba(255, 255, 0, 69), stop:1 rgba(255, 255, 0, 69));"))
        self.ServerName.setObjectName(_fromUtf8("ServerName"))

        self.CurrentVersionLabel = QtGui.QLabel(Form)
        self.CurrentVersionLabel.setGeometry(QtCore.QRect(800, 400, 61, 16))
        self.CurrentVersionLabel.setStyleSheet(_fromUtf8("color: rgb(145, 145, 145);"))
        self.CurrentVersionLabel.setObjectName(_fromUtf8("CurrentVersionLabel"))

        self.LatestVersionLabel = QtGui.QLabel(Form)
        self.LatestVersionLabel.setGeometry(QtCore.QRect(800, 380, 61, 16))
        self.LatestVersionLabel.setStyleSheet(_fromUtf8("color: rgb(145, 145, 145);"))
        self.LatestVersionLabel.setObjectName(_fromUtf8("LatestVersionLabel"))

        self.ServerStatusLabel = QtGui.QLabel(Form)
        self.ServerStatusLabel.setGeometry(QtCore.QRect(100, 80, 71, 16))
        self.ServerStatusLabel.setStyleSheet(_fromUtf8("color: rgb(129, 129, 129);\n"
"background: none;"))
        self.ServerStatusLabel.setObjectName(_fromUtf8("ServerStatusLabel"))

        self.OnlineLabel = QtGui.QLabel(Form)
        self.OnlineLabel.setGeometry(QtCore.QRect(80, 100, 101, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.OnlineLabel.setFont(font)
        self.OnlineLabel.setStyleSheet(_fromUtf8("color: rgb(91, 170, 0);"))
        self.OnlineLabel.setObjectName(_fromUtf8("OnlineLabel"))

        self.HorizontalLine = QtGui.QFrame(Form)
        self.HorizontalLine.setGeometry(QtCore.QRect(80, 90, 101, 20))
        font = QtGui.QFont()
        font.setPointSize(1)
        self.HorizontalLine.setFont(font)
        self.HorizontalLine.setStyleSheet(_fromUtf8("background: none;"))
        self.HorizontalLine.setFrameShape(QtGui.QFrame.HLine)
        self.HorizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.HorizontalLine.setObjectName(_fromUtf8("HorizontalLine"))

        self.pushButton = QtGui.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(720, 50, 161, 71))
        self.pushButton.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.pushButton.setStyleSheet(_fromUtf8("border-image: url(:/images/Settings/CSImages/left_main_snsImg100408.jpg);"))
        self.pushButton.setText(_fromUtf8(""))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))

        self.Closebutton = QtGui.QPushButton(Form)
        self.Closebutton.setGeometry(QtCore.QRect(20, 20, 40, 40))
        self.Closebutton.setCursor(QtGui.QCursor(QtCore.Qt.ClosedHandCursor))
        self.Closebutton.setStyleSheet(_fromUtf8("QPushButton{border-image: url(:/images/Settings/CSImages/closetekster.png);\n"
"\n"
"\n"
"background: transparent;}\n"
"\n"
"QPushButton:hover{\n"
"\n"
"border-image: url(:/images/Settings/CSImages/hover.png);\n"
"}"))
        self.Closebutton.setText(_fromUtf8(""))
        self.Closebutton.setObjectName(_fromUtf8("Closebutton"))

        self.MinimizeButton = QtGui.QPushButton(Form)
        self.MinimizeButton.setGeometry(QtCore.QRect(20, 60, 25, 29))
        self.MinimizeButton.setCursor(QtGui.QCursor(QtCore.Qt.ClosedHandCursor))
        self.MinimizeButton.setStyleSheet(_fromUtf8("\n"
"\n"
"QPushButton{border-image:  url(:/images/Settings/CSImages/minimizetekster.png);\n"
"\n"
"\n"
"background: transparent;}\n"
"\n"
"QPushButton:hover{\n"
"\n"
"border-image: url(:/images/Settings/CSImages/MinHoverTekster.png);\n"
"}"))
        self.MinimizeButton.setText(_fromUtf8(""))
        self.MinimizeButton.setObjectName(_fromUtf8("MinimizeButton"))

        self.Wizard = QtGui.QLabel(Form)
        self.Wizard.setGeometry(QtCore.QRect(0, 110, 291, 341))
        self.Wizard.setStyleSheet(_fromUtf8("background: transparent;"))
        self.Wizard.setText(_fromUtf8(""))
        _xIcon = self.Getter("BeastLauncher","Icon")
        self.Wizard.setPixmap(QtGui.QPixmap(_fromUtf8(_xIcon)))
        self.Wizard.setScaledContents(True)
        self.Wizard.setObjectName(_fromUtf8("Wizard"))

        self.CheckForUpdateButton = QtGui.QPushButton(Form)
        self.CheckForUpdateButton.setGeometry(QtCore.QRect(190, 400, 111, 41))
        self.CheckForUpdateButton.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.CheckForUpdateButton.setStyleSheet(_fromUtf8("QPushButton{\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #3498db, stop: 1 #2980b9);\n"
"color: #ffffff;\n"
"border: 2px outset #333751;\n"
"border-radius: 5px 5px 5px 5px;\n"
"}\n"
"\n"
"\n"
"QPushButton:hover\n"
"{\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2980b9, stop: 1 #3498db);\n"
"\n"
"border: none;\n"
"\n"
"}\n"
"\n"
""))
        self.CheckForUpdateButton.setObjectName(_fromUtf8("CheckForUpdateButton"))

        self.tabWidget = QtGui.QTabWidget(Form)
        self.tabWidget.setGeometry(QtCore.QRect(310, 350, 391, 111))
        self.tabWidget.setStyleSheet(_fromUtf8("QTabWidget::tab-bar {\n"
"background: none;\n"
"border: transparent;\n"
"border-color: transparent;\n"
"\n"
" }\n"
"\n"
" QTabBar::tab {\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #26293B , stop: 1 #26293B);\n"
"  color: white;\n"
"border: none;\n"
"  padding: 10px;\n"
" }\n"
"\n"
" QTabBar::tab:selected {\n"
"  background: orange;\n"
"border: none;\n"
" }\n"
"\n"
"\n"
"QTabWidget::pane { border: 0; }"))
        self.tabWidget.setTabPosition(QtGui.QTabWidget.North)
        self.tabWidget.setTabShape(QtGui.QTabWidget.Rounded)
        self.tabWidget.setElideMode(QtCore.Qt.ElideNone)
        self.tabWidget.setUsesScrollButtons(False)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setMovable(True)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))

        self.PlayNow = QtGui.QWidget()
        self.PlayNow.setObjectName(_fromUtf8("PlayNow"))

        self.StartGameButton_5 = QtGui.QPushButton(self.PlayNow)
        self.StartGameButton_5.setGeometry(QtCore.QRect(210, 20, 161, 41))
        self.StartGameButton_5.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.StartGameButton_5.setStyleSheet(_fromUtf8("border-image: url(:/images/Settings/CSImages/bt_gamestart.gif);"))
        self.StartGameButton_5.setText(_fromUtf8(""))
        self.StartGameButton_5.setObjectName(_fromUtf8("StartGameButton_5"))

        self.NewsInStartGame_5 = QtGui.QLabel(self.PlayNow)
        self.NewsInStartGame_5.setGeometry(QtCore.QRect(10, 0, 131, 71))

        font = QtGui.QFont()
        font.setPointSize(7)

        self.NewsInStartGame_5.setFont(font)
        self.NewsInStartGame_5.setStyleSheet(_fromUtf8("background: none;\n"
"color: #ffffff;"))
        self.NewsInStartGame_5.setObjectName(_fromUtf8("NewsInStartGame_5"))

        self.OrangeLetterC_5 = QtGui.QLabel(self.PlayNow)
        self.OrangeLetterC_5.setGeometry(QtCore.QRect(160, 30, 21, 16))
        self.OrangeLetterC_5.setText(_fromUtf8(""))
        self.OrangeLetterC_5.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/Settings/CSImages/icon_wcoinc.gif")))
        self.OrangeLetterC_5.setObjectName(_fromUtf8("OrangeLetterC_5"))

        self.tabWidget.addTab(self.PlayNow, _fromUtf8(""))

        self.SettingsTab = QtGui.QWidget()
        self.SettingsTab.setObjectName(_fromUtf8("SettingsTab"))

        self.SaveUsernameButton_5 = QtGui.QPushButton(self.SettingsTab)
        self.SaveUsernameButton_5.setGeometry(QtCore.QRect(250, 10, 131, 51))
        self.SaveUsernameButton_5.setStyleSheet(_fromUtf8("QPushButton{\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ffcc00, stop: 1 #b87f2b);\n"
"color: #ffffff;\n"
"border: none;\n"
"}\n"
"\n"
"\n"
"QPushButton:hover\n"
"{\n"
"background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2980b9, stop: 1 #3498db);\n"
"\n"
"border: none;\n"
"\n"
"}\n"
"\n"
""))
        self.SaveUsernameButton_5.setObjectName(_fromUtf8("SaveUsernameButton_5"))

        self.label_33 = QtGui.QLabel(self.SettingsTab)
        self.label_33.setGeometry(QtCore.QRect(10, 40, 71, 16))
        self.label_33.setStyleSheet(_fromUtf8("color: #ffffff;\n"
"background: none;"))
        self.label_33.setObjectName(_fromUtf8("label_33"))

        self.label_34 = QtGui.QLabel(self.SettingsTab)
        self.label_34.setGeometry(QtCore.QRect(140, 40, 91, 16))
        self.label_34.setStyleSheet(_fromUtf8("color: #ffffff;\n"
"background: none;"))
        self.label_34.setObjectName(_fromUtf8("label_34"))

        self.WindowsOnorFullSlider = QtGui.QSlider(self.SettingsTab)
        self.WindowsOnorFullSlider.setGeometry(QtCore.QRect(90, 40, 41, 19))
        self.WindowsOnorFullSlider.setStyleSheet(_fromUtf8("background: none;"))
        self.WindowsOnorFullSlider.setMaximum(2)
        self.WindowsOnorFullSlider.setValue(1)
        self.WindowsOnorFullSlider.setOrientation(QtCore.Qt.Horizontal)
        self.WindowsOnorFullSlider.setObjectName(_fromUtf8("WindowsOnorFullSlider"))
        self.WindowsOnorFullSlider.setDisabled(1)

        self.comboBox_5 = QtGui.QComboBox(self.SettingsTab)
        self.comboBox_5.setGeometry(QtCore.QRect(10, 10, 171, 22))
        self.comboBox_5.setStyleSheet(_fromUtf8("QComboBox { \n"
"     border: 1px solid gray;\n"
"     border-radius: 3px;\n"
"     padding: 1px 18px 1px 3px;\n"
"     min-width: 6em;\n"
"color:rgb(79, 79, 79);\n"
" }\n"
"\n"
" QComboBox:editable {\n"
"     background: white;\n"
" }\n"
"\n"
" QComboBox:!editable, QComboBox::drop-down:editable {\n"
"      background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                  stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                  stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
" }\n"
"\n"
" /* QComboBox gets the \"on\" state when the popup is open */\n"
" QComboBox:!editable:on, QComboBox::drop-down:editable:on {\n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                 stop: 0 #D3D3D3, stop: 0.4 #D8D8D8,\n"
"                                 stop: 0.5 #DDDDDD, stop: 1.0 #E1E1E1);\n"
" }\n"
"\n"
" QComboBox:on { /* shift the text when the popup opens */\n"
"     padding-top: 3px;\n"
"     padding-left: 4px;\n"
" }\n"
"\n"
" QComboBox::drop-down {\n"
"     subcontrol-origin: padding;\n"
"     subcontrol-position: top right;\n"
"     width: 15px;\n"
"     background: #555;\n"
"     border-left-width: 1px;\n"
"     border-left-color: darkgray;\n"
"     border-left-style: solid; /* just a single line */\n"
"     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
"     border-bottom-right-radius: 3px;\n"
" }\n"
"QComboBox QAbstractItemView {\n"
"    border: 2px solid darkgray;\n"
"    color: black;\n"
"background: yellow;\n"
"selection-color: black;\n"
"    selection-background-color: grey;\n"
"}\n"
"\n"
""))
        self.comboBox_5.setObjectName(_fromUtf8("comboBox_5"))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))
        self.comboBox_5.addItem(_fromUtf8(""))

        self.label = QtGui.QLabel(self.SettingsTab)
        self.label.setGeometry(QtCore.QRect(30, 60, 51, 16))
        self.label.setStyleSheet(_fromUtf8("color: #ffffff;\n"
"background: none;"))
        self.label.setObjectName(_fromUtf8("label"))

        self.label_2 = QtGui.QLabel(self.SettingsTab)
        self.label_2.setGeometry(QtCore.QRect(140, 60, 71, 16))
        self.label_2.setStyleSheet(_fromUtf8("color: #ffffff;\n"
"background: none;"))
        self.label_2.setObjectName(_fromUtf8("label_2"))

        self.SoundsOnandOffSlider = QtGui.QSlider(self.SettingsTab)
        self.SoundsOnandOffSlider.setGeometry(QtCore.QRect(90, 60, 41, 19))
        self.SoundsOnandOffSlider.setStyleSheet(_fromUtf8("color: #ffffff;\n"
"background: none;"))
        self.SoundsOnandOffSlider.setMaximum(2)
        self.SoundsOnandOffSlider.setValue(1)
        self.SoundsOnandOffSlider.setOrientation(QtCore.Qt.Horizontal)
        self.SoundsOnandOffSlider.setObjectName(_fromUtf8("SoundsOnandOffSlider"))

        self.tabWidget.addTab(self.SettingsTab, _fromUtf8(""))

        self.ContactTab = QtGui.QWidget()
        self.ContactTab.setObjectName(_fromUtf8("ContactTab"))

        self.label_36 = QtGui.QLabel(self.ContactTab)
        self.label_36.setGeometry(QtCore.QRect(10, 10, 371, 71))

        font = QtGui.QFont()
        font.setPointSize(7)

        self.label_36.setFont(font)
        self.label_36.setStyleSheet(_fromUtf8("color: rgb(166, 166, 166);\n"
"background: none;"))
        self.label_36.setObjectName(_fromUtf8("label_36"))


        self.progressBar = QtGui.QProgressBar(Form) #// Progress Bar
        self.progressBar.setGeometry(QtCore.QRect(278, 331,420, 15))
        self.progressBar.setStyleSheet(_fromUtf8(" QProgressBar {\n"
"     border: 2px solid grey;\n"
"     border-radius: 5px;\n"
"    text-align: center;\n"
"    color: #404040;\n"
"    font-weight: bold;\n"
" }\n"
"\n"
" QProgressBar:chunk {\n"
"     background-color: #05B8CC;\n"
"     width: 20px;\n"
" }"))
        self.progressBar.setProperty("value", 100)
        self.progressBar.setTextVisible(True)
        self.progressBar.setInvertedAppearance(False)
        self.progressBar.setTextDirection(QtGui.QProgressBar.TopToBottom)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))


        self.tabWidget.addTab(self.ContactTab, _fromUtf8(""))

        self.retranslateUi(Form)

        self.tabWidget.setCurrentIndex(0)

        QtCore.QObject.connect(self.WebshopButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.WebshopWebFunction)
        QtCore.QObject.connect(self.SMSCreditsButtons, QtCore.SIGNAL(_fromUtf8("clicked()")), self.SMSwebFunction)
        QtCore.QObject.connect(self.VoteButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.VoteButtonFunction)
        QtCore.QObject.connect(self.CheckForUpdateButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.CheckforUpdatesTrigger)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.SocialsBarFunction)
        QtCore.QObject.connect(self.Closebutton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.close)
        QtCore.QObject.connect(self.MinimizeButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.showMinimized)
        QtCore.QObject.connect(self.StartGameButton_5, QtCore.SIGNAL(_fromUtf8("clicked()")), self.StartGameFunction)
        QtCore.QObject.connect(self.SaveUsernameButton_5, QtCore.SIGNAL(_fromUtf8("clicked()")), self.SaveWindowsandSoundsFunction)

        QtCore.QMetaObject.connectSlotsByName(Form)

    class BeastErr(Exception):
        pass

    def BeastException(self,title,message):
        msgBox = QtGui.QMessageBox()
        msgBox.setWindowTitle(str(title))
        msgBox.setText(str(message))
        msgBox.exec_()


    def retranslateUi(self, Form):

        import urllib
        Form.setWindowTitle(_translate("Form", "BeastLauncher", None))
        self.LatestVersionName.setText(_translate("Form", "  Latest Version:", None))
        self.YourVersionName.setText(_translate("Form", "  Your Version:  ", None))
        self.StatusName.setText(_translate("Form", "  Status: ", None))
        readup = self.Getter('BeastUpdater','Updater')
        lwebversion = urllib.urlopen(str(readup)+'/csupdater/LatestVersion.txt')
        self.readwebver = lwebversion.read()
        self.Current_V = str(self.Getter('BeastUpdater','Version')).split(",")[-1] # Loads the last version

        try:
            if float(self.readwebver) > float(self.Current_V):
                self.label_11.setText('Please Update!')

            else:
                self.label_11.setText('Updated!')
        except:
            self.BeastException(title="BeastErr",message="Unable to Parse and Load Version")

        GetName = lambda: "" if self.Getter('BeastLauncher','Name') == 'None' else self.Getter('BeastLauncher','Name')
        self.ServerName.setText(_translate("Form", GetName(), None))
        self.CheckForUpdateButton.setText(_translate("Form", "Check for Updates", None))
        self.CurrentVersionLabel.setText(_translate("Form", "              " + str(self.Current_V), None))


        self.LatestVersionLabel.setText(_translate("Form", "              " + str(self.readwebver), None))

        self.ServerStatusLabel.setText(_translate("Form", "Server Status", None))

        self.OnlineLabel.setText(_translate("Form", "       ONLINE", None))

        self.NewsInStartGame_5.setText(_translate("Form", "<html><head/><body><p><span style=\" color:#949494;\">You might want to take a check</span></p><p><span style=\" color:#949494;\">first at the Settings tab before</span></p><p><span style=\" color:#949494;\">Starting the Game?</span></p></body></html>", None))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.PlayNow), _translate("Form", "Play Now", None))

        self.SaveUsernameButton_5.setText(_translate("Form", "Save Settings", None))

        self.label_33.setText(_translate("Form", "Windows Mode", None))

        self.label_34.setText(_translate("Form", "Full Screen Mode", None))

        self.comboBox_5.setCurrentIndex(3)
        self.comboBox_5.setItemText(0, _translate("Form", "     800x600  Window Screen", None))
        self.comboBox_5.setItemText(1, _translate("Form", "     1024x768  Window Screen", None))
        self.comboBox_5.setItemText(2, _translate("Form", "     1280x1024  Window Screen", None))
        self.comboBox_5.setItemText(3, _translate("Form", "     Select Window Size", None))
        self.comboBox_5.setItemText(4, _translate("Form", "     800x600  Full Screen", None))
        self.comboBox_5.setItemText(5, _translate("Form", "     1024x768 Full Screen", None))
        self.comboBox_5.setItemText(6, _translate("Form", "     1280x1024 Full Screen", None))

        self.label.setText(_translate("Form", "Sounds On", None))

        self.label_2.setText(_translate("Form", "Sounds Off", None))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.SettingsTab), _translate("Form", "Settings", None))
        try:
            SMess = self.Getter("BeastLauncher","Info").split(",")
            self.label_36.setText(_translate("Form", "<html><head/><body><p><b>{msg} </p></b><p>Visit Us for more infromations at     <u>{url}</u></p><br/>Copyright 2015 © CoreSEC Softwares, Ca<br/></body></html>".format(url=SMess[0],msg=SMess[1]), None))
        except:
            self.BeastException(title="BeastError",message="Unable to Parse [Info] value!")
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.ContactTab), _translate("Form", "Contact Us", None))


    def UpdateEngine(self,version,newversion):
        import urllib2 as get
        global TupleVersion, app, showed
        __version = version #// float from file
        __newver = newversion #// float from Web
        __updater = self.Getter("BeastUpdater","updater")
        __bindLoad = self.progressBar
        self.Threader = QtCore.QThread()
        self.Booster = QtGui.QApplication.processEvents
        showed = False
        def ListVersions(old,new,updater):
            "Lists of Versions to Update"
            global TupleVersion, TupleVersionWrite
            Data = (old,new,updater)
            TupleVersion = []         # Contains the Unwritten (Unseen Version)
            TupleVersionWrite = [str(float(old))] # Contains the List of Final Version (For Writing to Settings)

            def RemoveAllZips():
                import os
                global TupleVersion

                for cv in sorted(TupleVersion):
                    os.remove(str(cv)+".zip")

            def UnzipAll():
                import zipfile
                global TupleVersion
                for zips in sorted(TupleVersion):
                    CSunzip = zipfile.ZipFile(str(zips)+'.zip')
                    CSunzip.extractall()
                    CSunzip.close()
                RemoveAllZips()


            def ProcessAll(*args):
                "Processing Function for Datas and Versioning"
                global TupleVersionWrite,TupleVersion
                __bindLoad.setFormat("Locating versions to Patch!")

                def Message(m):
                    __bindLoad.setFormat(m)

                def SetVersion():
                    global TupleVersionWrite
                    import ConfigParser, sys
                    templ = ",".join(TupleVersionWrite)

                    ffile = "Settings/Main"
                    config = ConfigParser.ConfigParser()
                    config.read(ffile)
                    config.set("BeastUpdater","version",str(templ))
                    with open(ffile, "wb") as config_file:
                        config.write(config_file)


                def RevertStyleSheet(finished=False):
                    if finished:
                        __bindLoad.setStyleSheet("color: #404040;")
                    else:
                        __bindLoad.setStyleSheet("color: white ")

                def StartMain():
                    toStart2 = self.Getter("BeastLauncher","Executable")

                self.Booster()
                def StartUpdate(which=None,multi=False):
                    if multi:  # // Multiple version to patch
                        try:
                            webFolder = str(__updater)+'/csupdater/Versions/'+str(which)+'/'+str(which)+'.zip' # // e.g domain.com/csupdater/3.2/3.2.zip
                            url = webFolder
                            file_name = url.split('/')[-1]
                            u = get.urlopen(url)
                            f = open(file_name, 'wb')
                            meta = u.info()
                            file_size = int(meta.getheaders("Content-Length")[0])
                            file_size_dl = 0
                            block_sz = 8192
                            stop = False
                            self.Booster()
                            show_once = False

                            while not stop:
                                self.Booster()
                                if not stop and show_once == False:
                                    time.sleep(0.2)
                                    Message("Updater Started for version {v}".format(v=which))
                                    show_once = True

                                buffer = u.read(block_sz)

                                if not buffer:
                                    break

                                file_size_dl += len(buffer)
                                f.write(buffer)
                                status = r"%3.2f % % " % (file_size_dl * 100. / file_size)
                                #statuss = status + chr(8)*(len(status)+1)
                                x=status.replace('%','')
                                floatingX = float(x)
                                self.Booster()
                                Message("%p% for version {v}".format(v=which))
                                __bindLoad.setValue(float(floatingX))
                                self.Booster()
                                if __bindLoad.value() == 100:
                                    #f.close() #// Needs to Close (could be a MemLeak)
                                    stop = True #// Stops Loop
                                    self.label_11.setText("Updating..")
                                    self.CurrentVersionLabel.setText(_translate("Form", "              " + str(which), None))

                                    Message("Finished Updating to version {v}!".format(v=which))
                                    time.sleep(3)
                            f.close()
                        except:
                            Message("Unable to Update!")

                    else:
                        pass


                def CallRestarter():
                    pass


                def Evaluate():
                    global TupleVersion, TupleVersionWrite, showed
                    if TupleVersion:
                        self.Booster()
                        for ver in sorted(TupleVersion):
                            if len(TupleVersion) > 1 and showed == False:
                                Message("There are {cout} patches to update!".format(cout=len(TupleVersion)))
                                self.Booster()
                                showed = True
                                time.sleep(4)


                            else:
                                if not showed:
                                    Message("There is {cout} patch to update!".format(cout=len(TupleVersion)))
                                    self.Booster()
                                    time.sleep(4)
                                    showed = True

                            self.Booster()
                            StartUpdate(which=ver,multi=True)
                        Message("You can now start the Game!")
                        self.label_11.setText("Now Updated!")
                        UnzipAll() #// Unpack the Zipped patches
                        SetVersion() #// Write the Version
                Evaluate()

            def ProcVersions(*args):
                "Processing Function for Listing to Update Version"
                global TupleVersion, TupleVersionWrite
                import time

                old, new = eval(args[0]),args[1] #// Versions
                RealTStep = 0.1
                # // Always make the Step 0.1 (Else Patching will be too messy.)
                try:
                    while float(old) < float(new):
                        self.Booster()

                        time.sleep(0.1)
                        old += float(RealTStep)
                        TupleVersion.append(eval(str(old)))
                        TupleVersionWrite.append(str(eval(str(old))))
                        self.Booster()
                finally:
                    ProcessAll()

            def GetAhead():
                req = get.Request(str(Data[2])+'/csupdater/LatestVersion.txt')
                req.add_header('Referer', 'http://www.python.org/')
                resp = get.urlopen(req)
                GetLatest = resp.read()
                Latest = eval(str(GetLatest))
                ProcVersions(Data[0],Data[1]) # Process Version into Tupled List
            GetAhead()



        ListVersions(__version,__newver,__updater)  # Evalucate Version

    def WebshopWebFunction(self):
        strfile = self.Getter("BeastExternal","WebshopLink")
        rightnews = self.WebViewNewsEvents
        rightnews.setUrl(QtCore.QUrl(_fromUtf8(str(strfile))))
        rightnews.setGeometry(QtCore.QRect(190, 70, 500, 251))

    def SMSwebFunction(self):
        strfile = self.Getter("BeastExternal","SmsLink")
        rightnewsc = self.WebViewNewsEvents
        rightnewsc.setUrl(QtCore.QUrl(_fromUtf8(str(strfile))))
        rightnewsc.setGeometry(QtCore.QRect(190, 70, 500, 251))

    def SocialsBarFunction(self):
        strfile = self.Getter("BeastExternal","SocialLink")
        rightnewsq = self.WebViewNewsEvents
        rightnewsq.setUrl(QtCore.QUrl(_fromUtf8(str(strfile))))
        rightnewsq.setGeometry(QtCore.QRect(190, 70, 500, 251))

    def VoteButtonFunction(self):
        strfile = self.Getter("BeastExternal","VoteLink")
        rightnewsx = self.WebViewNewsEvents
        rightnewsx.setUrl(QtCore.QUrl(_fromUtf8(str(strfile))))
        rightnewsx.setGeometry(QtCore.QRect(190, 70, 500, 251))

    def CheckforUpdatesTrigger(self):
        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.CheckforUpdatesFunction)
        self.timer.start(1000)

    def CheckforUpdatesFunction(self,get=False):
        try:
            Getupdater = self.Getter("BeastUpdater","Updater")
            req = urllib2.Request(str(Getupdater)+'/csupdater/LatestVersion.txt')
            req.add_header('Referer', 'http://www.python.org/')
            resp = urllib2.urlopen(req)
            GetLatest = resp.read()   # Get Web Version / Latest
            floatWeb = float(GetLatest) # Web Version
        except:
            self.BeastException("UpdateEngine Failed","Connection or Remote file not found.")

        EvaledFloat = eval(str(self.CurrentVersionLabel.text()))
        if get:
            if float(floatWeb) > float(EvaledFloat):
                return True
            else:
                return False

        if not get:
            if float(floatWeb) > float(EvaledFloat):
                self.UpdateEngine(self.Current_V,floatWeb)
            else:
                self.progressBar.setFormat("Your client is Updated already!")

    def StartGameFunction(self):
        try:
            import BeastAH
        except:
            pass

        from subprocess import Popen

        if self.CheckforUpdatesFunction(get=True):
            self.CheckforUpdatesFunction()

        else:
            try:
                Form.close(),Popen(str(self.Getter("BeastLauncher","Executable")))  # Runs Game if no Latest Version
            except:
                self.BeastException("Error Loading Main","Unable to Load Main Executable!.")

    def SaveUsernameFunction(self):
        """  Unused Function """
        pass # hahaha lucky you

    def SaveWindowsandSoundsFunction(self):
        from subprocess import Popen as run
        getResolutions = self.comboBox_5.currentText()  #Get Resolution
        setWindSlider = self.WindowsOnorFullSlider
        getSounds = self.SoundsOnandOffSlider.value()
        Server = self.Getter("BeastLauncher","Name")
        if Server == "0" or Server == "None":
            Server = ""
        _str = """regedit.exe /s "Settings/Resolution/"""
        ######## WS START ############
        if "Select Window Size" in str(getResolutions):
            setWindSlider.setValue(0)

            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: Default Selected')

        if "800x600  Window Screen" in str(getResolutions):
            setWindSlider.setValue(0)
            run(_str+"w800x600.reg", shell=True)  #window 8x6
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 800x600 Window-Mode Selected [PRESS YES]')
        if "1024x768  Window Screen" in str(getResolutions):
            setWindSlider.setValue(0)
            run(_str+"w1024x768.reg", shell=True)  #window 1x7
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 1024x768 Window-Mode Selected [PRESS YES]')
        if "1280x1024  Window Screen" in str(getResolutions):
            setWindSlider.setValue(0)
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 1280x1024 Window-Mode Selected [PRESS YES]')
            run(_str+"w1280x1024.reg", shell=True) #window 121


        ############## FS START #######
        if "800x600  Full Screen" in str(getResolutions):
            setWindSlider.setValue(2)
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 800x600 Full-Screen Mode Selected [PRESS YES]')
            run(_str+'f800x600.reg', shell=True) #window 8x6

        if "1024x768 Full Screen" in str(getResolutions):
            setWindSlider.setValue(2)
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 1024x768 Full-Screen Mode Selected [PRESS YES]')
            run(_str+'f1024x768.reg', shell=True) #window 8x6

        if "1280x1024 Full Screen" in str(getResolutions):
            setWindSlider.setValue(2)
            self.NotificationBox.clear(), self.NotificationBox2.clear(),self.NotificationBox2.insertPlainText(str(Server)+' Launcher: 1280x1024 Full-Screen Mode Selected [PRESS YES]')
            run(_str+'f1280x1024.reg', shell=True) #window 8x6
        ##############################################

        def CheckSoundsNow(object):
            getsSounds = getSounds
            if getsSounds == 1:
                pass # Next Update
            if getsSounds == 2:
                pass # Next Update
            if getsSounds == 0:
                pass # Next Update
        CheckSoundsNow(object)

    def close(self):
        Form.close()

    def showMinimized(self):
        Form.showMinimized()

import bl
from PyQt4 import QtWebKit
if __name__ == "__main__":
    import sys,time
    global app
    app = QtGui.QApplication(sys.argv)

    def Proc_Splash(get=False):
        import ConfigParser
        P = ConfigParser.ConfigParser()
        P.read("Settings/Main")
        try:
            Booled = P.get("BeastLauncher","Splash").split(",")
            if get:
                if Booled[0] == "0" or Booled[0] == "None" or Booled[0] == "False":
                    return False
                else:
                    return True
            else:
                return Booled[1]
        except:
            pass    # splash Err

    #splasher
    if Proc_Splash(get=True) == True:
        splash_pix = QtGui.QPixmap(Proc_Splash(get=False))
        splash = QtGui.QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        splash.setMask(splash_pix.mask())
        splash.show()

    else:
        pass
    # end
    time.sleep(2)

    if Proc_Splash(get=True) == True:
        app.processEvents()
    Form = QtGui.QWidget()
    Form.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    if Proc_Splash(get=True) == True:
        splash.finish(Form)
    sys.exit(app.exec_())

