# -*- mode: python -*-
a = Analysis(['Launcher.py'],
             pathex=['C:\\Program Files (x86)\\Lib\\site-packages\\PyQt4', 'C:\\Users\\Mark\\Desktop\\Sources\\BeastLauncher'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Launcher.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , version='version.txt', icon='Settings\\CSImages\\icon.ico')
